package application.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import application.model.Betaling;
import application.model.Betalingsmetode;
import application.model.Fastkunde;
import application.model.Pantvare;
import application.model.Pris;
import application.model.Prisliste;
import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Rundvisning;
import application.model.Salg;
import application.model.Salgslinje;
import application.model.Sampakning;
import storage.Storage;

public class Service {
	private static Service service;
	private Storage storage;

	public static Service getService() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	private Service() {
		storage = Storage.getStorage();
	}

	/**
	 * Opretter produktgruppe
	 * 
	 * @param navn
	 * @return produktgruppe
	 */
	public Produktgruppe createProduktgruppe(String navn) {
		Produktgruppe produktgruppe = new Produktgruppe(navn);
		storage.addProduktgruppe(produktgruppe);
		return produktgruppe;
	}

	public List<Produktgruppe> getProduktgrupper() {
		return storage.getProduktgrupper();
	}

	public Salg createSalg() {
		Salg salg = new Salg();
		return salg;
	}

	public List<Salg> getSalg() {
		return new ArrayList<>(storage.getSalgsliste());
	}

	/**
	 * Færdiggøre salg og gemmer på ArrayList
	 * 
	 * @param salg
	 */
	public void finishSalg(Salg salg) {
		storage.addSalg(salg);
	}

	/**
	 * Opretter prisliste med navn
	 * 
	 * @param navn
	 * @return prisliste
	 */
	public Prisliste createPrisliste(String navn) {
		Prisliste prisliste = new Prisliste(navn);
		storage.addPrisliste(prisliste);
		return prisliste;
	}

	public List<Prisliste> getPrislister() {
		return storage.getPrislister();
	}

	/**
	 * Opretter fastkunde med navn og en rabat
	 * 
	 * @param navn
	 * @param rabat
	 * @return fastkunde
	 */
	public Fastkunde createFastkunde(String navn, double rabat) {
		Fastkunde fastkunde = new Fastkunde(navn, rabat);
		storage.addFastkunde(fastkunde);
		return fastkunde;
	}

	public List<Fastkunde> getFastekunder() {
		return storage.getFastkunder();
	}

	/**
	 * Opretter produkt med navn og produktgrupp
	 * 
	 * @param navn
	 * @param produktgruppe
	 * @return produkt
	 */
	public Produkt createProdukt(String navn, Produktgruppe produktgruppe) {
		Produkt produkt = new Produkt(navn);
		produkt.setProduktgruppe(produktgruppe);
		produktgruppe.addProdukt(produkt);
		return produkt;
	}

	/**
	 * Opretter pantvare med navn, pant og produktgruppe
	 * 
	 * @param navn
	 * @param pant
	 * @param produktgruppe
	 * @return
	 */
	public Pantvare createPantvare(String navn, double pant, Produktgruppe produktgruppe) {
		Pantvare pantvare = new Pantvare(navn, pant);
		pantvare.setProduktgruppe(produktgruppe);
		produktgruppe.addProdukt(pantvare);
		return pantvare;
	}

	/**
	 * Opretter rundvisning med dato, start tid, slut tid og antal deltagere
	 * Opretter pris med antal deltagere * 100
	 * 
	 * @param dato
	 * @param startTid
	 * @param slutTid
	 * @param deltagere
	 * @return
	 */
	public Rundvisning createRundvisning(LocalDate dato, LocalTime startTid, LocalTime slutTid, int deltagere) {
		Rundvisning rundvisning = new Rundvisning(dato, startTid, slutTid, deltagere);
		Pris pris = new Pris(100 * deltagere, 0, rundvisning);
		storage.addRundvisningspris(pris);
		return rundvisning;
	}

	public List<Pris> getRundvisninspriser() {
		return storage.getRundvisningspriser();
	}

	/**
	 * Opretter sampakning med navn og pris
	 * 
	 * @param navn
	 * @param normalPris
	 * @return
	 */
	public Sampakning createSampakning(String navn, double normalPris) {
		Sampakning sampakning = new Sampakning(navn);
		Pris pris = new Pris(normalPris, 0, sampakning);
		storage.addSampakningspris(pris);
		return sampakning;
	}

	public List<Pris> getSampakningspriser() {
		return storage.getSampakningspriser();
	}

	/**
	 * Opretter anlæg med navn og pris Anlæg gemmes i arraylist for sig selv
	 * 
	 * @param navn
	 * @param normalPris
	 * @return
	 */
	public Produkt createAnlæg(String navn, double normalPris) {
		Produkt anlæg = new Produkt(navn);
		Pris pris = new Pris(normalPris, 0, anlæg);
		storage.addAnlægspris(pris);
		return anlæg;
	}

	public List<Pris> getAnlægspriser() {
		return storage.getAnlægspriser();
	}

	public List<Salg> getUdlejninger() {
		return storage.getUdlejninger();
	}

	public void addUdlejning(Salg salg) {
		storage.addUdlejning(salg);
	}

	public void removeUdlejning(Salg salg) {
		storage.removeUdlejning(salg);
	}

	/**
	 * Retunere alle priser under prisliste hvis prisens produkt er det samme som
	 * produktgruppe
	 * 
	 * @param prisliste
	 * @param produktgruppe
	 * @return
	 */
	public List<Pris> getPriser(Prisliste prisliste, Produktgruppe produktgruppe) {
		List<Pris> priser = new ArrayList<>();
		for (Pris p : prisliste.getPriser()) {
			if (p.getProdukt().getProduktgruppe() != null && p.getProdukt().getProduktgruppe().equals(produktgruppe)) {
				priser.add(p);
			}
		}
		return priser;
	}

	/**
	 * Finder hvor mange af et produkt der er solgt over periode
	 * 
	 * @param startDato
	 * @param slutDato
	 * @param produkt
	 * @return
	 */
	public int solgtProdukt(LocalDate startDato, LocalDate slutDato, Produkt produkt) {
		int counter = 0;
		for (Salg sa : getSalg()) {
			if (sa.getDato().isAfter(startDato) || sa.getDato().equals(startDato) && sa.getDato().isBefore(slutDato)
					|| sa.getDato().equals(slutDato)) {
				for (Salgslinje sl : sa.getSalgslinjer()) {
					if (sl.getPris().getProdukt() instanceof Sampakning) {
						for (Produkt p : ((Sampakning) sl.getPris().getProdukt()).getProdukter()) {
							if (p.equals(produkt)) {
								counter++;
							}
						}
					} else if (sl.getPris().getProdukt().equals(produkt)) {
						counter += sl.getAntal();
					}
				}
			}
		}
		return counter;
	}

	/**
	 * Finder alle klippekort solgt over periode og ganger med 4
	 * 
	 * @param startDato
	 * @param slutDato
	 * @return
	 */
	public int solgteKlip(LocalDate startDato, LocalDate slutDato) {
		int solgteKlipCounter = 0;
		for (Salg sa : getSalg()) {
			if (sa.getDato().isAfter(startDato) || sa.getDato().equals(startDato) && sa.getDato().isBefore(slutDato)
					|| sa.getDato().equals(slutDato)) {
				for (Salgslinje sl : sa.getSalgslinjer()) {
					if (sl.getPris().getProdukt().getID() == 64) {
						solgteKlipCounter += sl.getAntal();
					}
				}
			}
		}
		return solgteKlipCounter * 4;
	}

	/**
	 * Finder brugte klip over en periode
	 * 
	 * @param startDato
	 * @param slutDato
	 * @return
	 */
	public int brugteKlip(LocalDate startDato, LocalDate slutDato) {
		int brugteKlipCounter = 0;
		for (Salg sa : getSalg()) {
			if (sa.getDato().isAfter(startDato) || sa.getDato().equals(startDato) && sa.getDato().isBefore(slutDato)
					|| sa.getDato().equals(slutDato)) {
				for (Betaling b : sa.getBetalinger()) {
					if (b.getBetalingsmetode() == Betalingsmetode.KLIPPEKORT) {
						for (Salgslinje sl : sa.getSalgslinjer()) {
							brugteKlipCounter += sl.getPris().getKlipPris() * sl.getAntal();
						}
					}
				}
			}
		}
		return brugteKlipCounter;
	}

	/**
	 * Finder alle salg på en given dato
	 */
	public List<Salg> salgStatistik(LocalDate dato) {
		List<Salg> salgsliste = new ArrayList<>();
		for (Salg sa : getSalg()) {
			if (sa.getDato().equals(dato)) {
				salgsliste.add(sa);
			}
		}
		return salgsliste;
	}

	/**
	 * Finder alle planlagte rundvisinger inden for datoer. Sortere rundvisninger
	 * efter dato
	 * 
	 * @param startDato
	 * @param slutDato
	 * @return
	 */
	public List<Rundvisning> getRundvisninger(LocalDate startDato, LocalDate slutDato) {
		List<Rundvisning> rundvisninger = new ArrayList<>();
		for (Pris p : getRundvisninspriser()) {
			Rundvisning r = (Rundvisning) p.getProdukt();
			if (r.getDato().isAfter(startDato) || r.getDato().equals(startDato) && r.getDato().isBefore(slutDato)
					|| r.getDato().equals(slutDato)) {
				rundvisninger.add(r);
			}
		}
		Collections.sort(rundvisninger);
		return rundvisninger;
	}

	public void initStorage() {
		Prisliste butik = createPrisliste("Butik");
		Prisliste fredagsbar = createPrisliste("Fredagsbar");

		Produktgruppe flaske = createProduktgruppe("Flaske");
		Produktgruppe fadøl = createProduktgruppe("Fadøl, 40 cl");
		Produktgruppe spiritus = createProduktgruppe("Spiritus");
		Produktgruppe fustage = createProduktgruppe("Fustage");
		Produktgruppe kultsyre = createProduktgruppe("Kultsyre");
		Produktgruppe malt = createProduktgruppe("Malt");
		Produktgruppe beklædning = createProduktgruppe("Beklædning");
		Produktgruppe glas = createProduktgruppe("Glas");
		Produktgruppe andet = createProduktgruppe("Andet");

		Produkt klosterbryg = createProdukt("Klosterbryg", flaske);
		Produkt sweetGerogiaBrown = createProdukt("Sweet Gerogia Brown", flaske);
		Produkt extraPilsner = createProdukt("Extra Pilsner", flaske);
		Produkt celebration = createProdukt("Celebration", flaske);
		Produkt blondie = createProdukt("Blondie", flaske);
		Produkt forårsbryg = createProdukt("Forårsbryg", flaske);
		Produkt indianaPaleAle = createProdukt("Indiana Pale Ale", flaske);
		Produkt julebryg = createProdukt("Julebryg", flaske);
		Produkt juletønden = createProdukt("Juletønden", flaske);
		Produkt oldStrongAle = createProdukt("Old Strong Ale", flaske);
		Produkt fregattenJylland = createProdukt("Fregatten Jylland", flaske);
		Produkt imperialstout = createProdukt("Imperial Stout", flaske);
		Produkt tribute = createProdukt("Tribute", flaske);
		Produkt blackMonster = createProdukt("Black Monster", flaske);

		butik.createPris(36, 0, klosterbryg);
		butik.createPris(36, 0, sweetGerogiaBrown);
		butik.createPris(36, 0, extraPilsner);
		butik.createPris(36, 0, celebration);
		butik.createPris(36, 0, blondie);
		butik.createPris(36, 0, forårsbryg);
		butik.createPris(36, 0, indianaPaleAle);
		butik.createPris(36, 0, julebryg);
		butik.createPris(36, 0, juletønden);
		butik.createPris(36, 0, oldStrongAle);
		butik.createPris(36, 0, fregattenJylland);
		butik.createPris(36, 0, imperialstout);
		butik.createPris(36, 0, tribute);
		butik.createPris(50, 0, blackMonster);

		fredagsbar.createPris(50, 2, klosterbryg);
		fredagsbar.createPris(50, 2, sweetGerogiaBrown);
		fredagsbar.createPris(50, 2, extraPilsner);
		fredagsbar.createPris(50, 2, celebration);
		fredagsbar.createPris(50, 2, blondie);
		fredagsbar.createPris(50, 2, forårsbryg);
		fredagsbar.createPris(50, 2, indianaPaleAle);
		fredagsbar.createPris(50, 2, julebryg);
		fredagsbar.createPris(50, 2, juletønden);
		fredagsbar.createPris(50, 2, oldStrongAle);
		fredagsbar.createPris(50, 2, fregattenJylland);
		fredagsbar.createPris(50, 2, imperialstout);
		fredagsbar.createPris(50, 2, tribute);
		fredagsbar.createPris(50, 2, blackMonster);

		Produkt klosterbryg2 = createProdukt("Klosterbryg", fadøl);
		Produkt jazzClassic = createProdukt("Jazz Classic", fadøl);
		Produkt extraPilnser2 = createProdukt("Extra Pilsner", fadøl);
		Produkt celebration2 = createProdukt("Celebration", fadøl);
		Produkt blondie2 = createProdukt("Blondie", fadøl);
		Produkt forårsbryg2 = createProdukt("Forårsbryg", fadøl);
		Produkt indianaPaleAle2 = createProdukt("Indiana Pale Ale", fadøl);
		Produkt julebryg2 = createProdukt("Julebryg", fadøl);
		Produkt imperialStout2 = createProdukt("Imperial Stout", fadøl);
		Produkt special = createProdukt("Special", fadøl);
		Produkt æblebrus = createProdukt("Æblebrus", fadøl);
		Produkt chips = createProdukt("Chips", fadøl);
		Produkt peanuts = createProdukt("Peanuts", fadøl);
		Produkt cola = createProdukt("Cola", fadøl);
		Produkt nikoline = createProdukt("Nikoline", fadøl);
		Produkt sevenUp = createProdukt("7-Up", fadøl);
		Produkt vand = createProdukt("Vand", fadøl);

		fredagsbar.createPris(30, 2, klosterbryg2);
		fredagsbar.createPris(30, 2, jazzClassic);
		fredagsbar.createPris(30, 2, extraPilnser2);
		fredagsbar.createPris(30, 2, celebration2);
		fredagsbar.createPris(30, 2, blondie2);
		fredagsbar.createPris(30, 2, forårsbryg2);
		fredagsbar.createPris(30, 2, indianaPaleAle2);
		fredagsbar.createPris(30, 2, julebryg2);
		fredagsbar.createPris(30, 2, imperialStout2);
		fredagsbar.createPris(30, 2, special);
		fredagsbar.createPris(15, 1, æblebrus);
		fredagsbar.createPris(10, 1, chips);
		fredagsbar.createPris(10, 1, peanuts);
		fredagsbar.createPris(15, 1, cola);
		fredagsbar.createPris(15, 1, nikoline);
		fredagsbar.createPris(15, 1, sevenUp);
		fredagsbar.createPris(10, 1, vand);

		Produkt spiritofAarhus = createProdukt("Spirit of Aarhus", spiritus);
		Produkt soaMedPind = createProdukt("SOA med pind", spiritus);
		Produkt whisky = createProdukt("Whisky", spiritus);
		Produkt liqourofAarhus = createProdukt("Liqour of Aarhus", spiritus);

		butik.createPris(300, 0, spiritofAarhus);
		butik.createPris(350, 0, soaMedPind);
		butik.createPris(500, 0, whisky);
		butik.createPris(175, 0, liqourofAarhus);

		fredagsbar.createPris(300, 0, spiritofAarhus);
		fredagsbar.createPris(350, 0, soaMedPind);
		fredagsbar.createPris(500, 0, whisky);
		fredagsbar.createPris(175, 0, liqourofAarhus);

		Produkt klosterbrygFustage = createPantvare("Kosterbryg, 20 liter", 200, fustage);
		Produkt jazzClassicFustage = createPantvare("Jazz Classic, 25 liter", 200, fustage);
		Produkt extraPilsnerFustage = createPantvare("Extra Pilsner, 25 liter", 200, fustage);
		Produkt celebrationFustage = createPantvare("Celebration, 20 liter", 200, fustage);
		Produkt blondieFustage = createPantvare("Bondie, 25 liter", 200, fustage);
		Produkt forårsbrygFustage = createPantvare("Forårsbryg, 20 liter", 200, fustage);
		Produkt indianaPaleAleFustage = createPantvare("Indiana Pale Ale, 20 liter", 200, fustage);
		Produkt julebrygFustage = createPantvare("Julebryg, 20 liter", 200, fustage);
		Produkt imperialStoutFustage = createPantvare("Imperial Stout, 20 liter", 200, fustage);

		butik.createPris(775, 0, klosterbrygFustage);
		butik.createPris(625, 0, jazzClassicFustage);
		butik.createPris(575, 0, extraPilsnerFustage);
		butik.createPris(775, 0, celebrationFustage);
		butik.createPris(700, 0, blondieFustage);
		butik.createPris(775, 0, forårsbrygFustage);
		butik.createPris(775, 0, indianaPaleAleFustage);
		butik.createPris(775, 0, julebrygFustage);
		butik.createPris(775, 0, imperialStoutFustage);

		Produkt sekskg = createPantvare("6 kg", 1000, kultsyre);
		Produkt firekg = createPantvare("4 kg", 1000, kultsyre);
		Produkt tikg = createPantvare("10 kg", 1000, kultsyre);

		butik.createPris(400, 0, sekskg);
		butik.createPris(270, 0, firekg);
		butik.createPris(670, 0, tikg);

		fredagsbar.createPris(400, 0, sekskg);
		fredagsbar.createPris(270, 0, firekg);
		fredagsbar.createPris(670, 0, tikg);

		Produkt malt25kg = createProdukt("25 kg sæk", malt);
		butik.createPris(300, 0, malt25kg);

		Produkt tShirt = createProdukt("t-shirt", beklædning);
		Produkt polo = createProdukt("polo", beklædning);
		Produkt cap = createProdukt("cap", beklædning);

		butik.createPris(70, 0, tShirt);
		butik.createPris(100, 0, polo);
		butik.createPris(30, 0, cap);

		fredagsbar.createPris(70, 0, tShirt);
		fredagsbar.createPris(100, 0, polo);
		fredagsbar.createPris(30, 0, cap);

		createAnlæg("1-hane", 250);
		createAnlæg("2-haner", 400);
		createAnlæg("Bar med flere haner", 500);
		createAnlæg("Levering", 500);
		createAnlæg("Krus", 60);

		Produkt glas2 = createProdukt("Glas", glas);
		butik.createPris(15, 1, glas2);

		createSampakning("gaveæske 2 øl, 2 glas", 100);
		createSampakning("gaveæske 4 øl", 130);
		createSampakning("trækasse 6 øl", 240);
		createSampakning("gavekurv 6 øl, 2 glas", 250);
		createSampakning("trækasse 6 øl, 6 glas", 290);
		createSampakning("trækasse 12 øl", 390);
		createSampakning("papkasse 12 øl", 360);

		Produkt klippekort = createProdukt("Klippekort, 4 klip", andet);

		butik.createPris(100, 0, klippekort);

		fredagsbar.createPris(100, 0, klippekort);

		createFastkunde("Kvickly", 10);
		createFastkunde("Rema 1000", 5);
	}
}
