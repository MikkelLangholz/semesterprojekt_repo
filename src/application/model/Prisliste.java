package application.model;

import java.util.ArrayList;
import java.util.List;

public class Prisliste {
	private String navn;
	private List<Pris> priser = new ArrayList<>();

	public Prisliste(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public List<Pris> getPriser() {
		return new ArrayList<>(priser);
	}

	/**
	 * Komposition. Opretter pris på prisliste
	 * 
	 * @param normalPris
	 * @param klipPris
	 * @param produkt
	 * @return pris
	 */
	public Pris createPris(double normalPris, int klipPris, Produkt produkt) {
		Pris pris = new Pris(normalPris, klipPris, produkt);
		priser.add(pris);
		return pris;
	}

	public void removePris(Pris pris) {
		priser.remove(pris);
	}

	@Override
	public String toString() {
		return navn;
	}
}
