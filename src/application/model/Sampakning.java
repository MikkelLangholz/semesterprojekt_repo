package application.model;

import java.util.ArrayList;
import java.util.List;

public class Sampakning extends Produkt {
	private List<Produkt> produkter = new ArrayList<>();

	public Sampakning(String navn) {
		super(navn);
	}

	public List<Produkt> getProdukter() {
		return new ArrayList<>(produkter);
	}

	public void addProdukt(Produkt produkt) {
		produkter.add(produkt);
	}

	public void removeProdukt(Produkt produkt) {
		produkter.remove(produkt);
	}
}