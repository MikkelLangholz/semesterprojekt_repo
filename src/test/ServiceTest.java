package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import application.model.Betalingsmetode;
import application.model.Pris;
import application.model.Produkt;
import application.model.Rundvisning;
import application.model.Salg;
import application.service.Service;

public class ServiceTest {
    private Salg solgtProduktTestSalg, klipSalg;
    private Produkt SolgtProduktTestProdukt, klipProdukt;
    private Pris SolgtProduktTestPris, klipPris;
    private Service service = Service.getService();

    @Before
    public void setUp() {
        solgtProduktTestSalg = new Salg();

        SolgtProduktTestProdukt = new Produkt("SolgtProduktTestProdukt");

        SolgtProduktTestPris = new Pris(36, 0, SolgtProduktTestProdukt);

        solgtProduktTestSalg.createSalgslinje(3, SolgtProduktTestPris);
        solgtProduktTestSalg.createSalgslinje(2, SolgtProduktTestPris);
        service.finishSalg(solgtProduktTestSalg);
    }

    @Test
    public void solgtProduktTest() {

        assertEquals(5,
                service.solgtProdukt(LocalDate.of(2018, 04, 23), LocalDate.of(2018, 05, 26), SolgtProduktTestProdukt));
    }

    @Test
    public void brugteKlipTest() {
        klipSalg = new Salg();
        klipProdukt = new Produkt("klipProdukt");
        klipPris = new Pris(0, 2, klipProdukt);
        klipSalg.createSalgslinje(2, klipPris);
        klipSalg.createBetaling(Betalingsmetode.KLIPPEKORT, 0);
        service.finishSalg(klipSalg);
        assertEquals(4, service.brugteKlip(LocalDate.now(), LocalDate.now()));

    }

    @Test
    public void createRundvisning() {
        service.createRundvisning(LocalDate.now(), LocalTime.of(14, 00), LocalTime.of(16, 00), 10);
        int længde = service.getRundvisninger(LocalDate.of(14, 1, 1), LocalDate.of(2020, 1, 1)).size();
        Rundvisning rundvisning = service.getRundvisninger(LocalDate.of(14, 1, 1), LocalDate.of(2020, 1, 1)).get(0);

        assertEquals(1, længde);
        assertEquals(rundvisning, service.getRundvisninger(LocalDate.of(14, 1, 1), LocalDate.of(2020, 1, 1)).get(0));
    }

}
