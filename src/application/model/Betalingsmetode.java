package application.model;

public enum Betalingsmetode {
	DANKORT, KONTANT, MOBILEPAY, REGNING, KLIPPEKORT
}
