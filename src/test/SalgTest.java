package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import application.model.Betaling;
import application.model.Betalingsmetode;
import application.model.Fastkunde;
import application.model.Pris;
import application.model.Produkt;
import application.model.Salg;
import application.model.Salgslinje;

public class SalgTest {

    private Fastkunde fastkunde1;
    private Fastkunde fastkundetest;
    private Salg salg1, salg2, salgBetalingsmetodeTest, calcManglendeOgSamletPrisTest, SalgManglendeBetalingTest;
    private Produkt produkt1, calcPrisProdukt;
    private Pris pris1, calcpristestPris, prisManglendeBetalingTest;

    @Before
    public void setUp() {
        salg1 = new Salg();
        salg2 = new Salg();
        salgBetalingsmetodeTest = new Salg();

        produkt1 = new Produkt("testprodukt");
        calcPrisProdukt = new Produkt("calcPrisproduktet");

        prisManglendeBetalingTest = new Pris(20, 0, produkt1);
        pris1 = new Pris(20, 0, produkt1);
        calcpristestPris = new Pris(125, 0, calcPrisProdukt);

        salg1.createSalgslinje(2, pris1);
        salg1.createSalgslinje(1, pris1);

        calcManglendeOgSamletPrisTest = new Salg();
        calcManglendeOgSamletPrisTest.createSalgslinje(3, calcpristestPris);

    }

    @Test
    public void setFastkundetest1() {

        salg1.setFastkunde(fastkunde1);
        assertEquals(fastkunde1, fastkundetest);

    }

    @Test
    public void getDatoTest() {
        LocalDate testdate = salg1.getDato();
        assertEquals(LocalDate.now(), testdate);

    }

    @Test
    public void getFastkundeTest() {

        salg1.setFastkunde(fastkunde1);
        Fastkunde testkunde = salg1.getFastkunde();
        assertEquals(fastkunde1, testkunde);

    }

    @Test
    public void getSamletPris() {
        salg2.createSalgslinje(2, pris1);
        salg2.calcSamletPris();
        assertEquals(40, salg2.getSamletPris(), 0.0001);

    }

    // Her testes setManglendePris()-metoden også, da prisen ikke ville være 200
    // hvis set-metoden ikke virkede.
    @Test
    public void getManglendePrisTest() {
        salg1.setManglendePris(200);

        assertEquals(200, salg1.getManglendePris(), 0.0001);
    }

    @Test
    public void getSalgslinjer() {
        Salgslinje salgslinjetest = salg1.getSalgslinjer().get(0);
        assertEquals(salgslinjetest, salg1.getSalgslinjer().get(0));
    }

    @Test
    public void createSalgslinjeTest() {
        Salgslinje salgslinjetest = salg1.createSalgslinje(1, pris1);
        Salgslinje salgslinje = salg1.getSalgslinjer().get(2);
        assertEquals(salgslinjetest, salgslinje);

    }

    @Test
    public void removeSalgslinje() {
        int startlængde = salg1.getSalgslinjer().size();
        salg1.removeSalgslinje(salg1.getSalgslinjer().get(1));
        int slutlængde = salg1.getSalgslinjer().size();
        assertEquals(2, startlængde);
        assertEquals(1, slutlængde);

    }

    @Test
    public void createBetaling() {
        salgBetalingsmetodeTest = new Salg();
        Betaling testbetaling = salgBetalingsmetodeTest.createBetaling(Betalingsmetode.DANKORT, 250);
        assertEquals(testbetaling, salgBetalingsmetodeTest.getBetalinger().get(0));

    }

    @Test
    public void getBetalingerTest() {

        salgBetalingsmetodeTest.createBetaling(Betalingsmetode.DANKORT, 250);
        Betaling betalingtest = salgBetalingsmetodeTest.getBetalinger().get(0);
        assertEquals(betalingtest, salgBetalingsmetodeTest.getBetalinger().get(0));
    }

    @Test
    public void calcSamletPrisTest() {
        double calcPris = calcManglendeOgSamletPrisTest.calcSamletPris();
        assertEquals(375, calcPris, 0.001);
    }

    @Test
    public void calcManglendePrisTest() {
        calcManglendeOgSamletPrisTest.createBetaling(Betalingsmetode.KONTANT, 125);

        assertEquals(250, calcManglendeOgSamletPrisTest.calcManglendePris(), 0.001);

    }

}
