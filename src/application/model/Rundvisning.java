package application.model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Rundvisning extends Produkt implements Comparable<Rundvisning> {
	private LocalDate dato;
	private LocalTime startTid;
	private LocalTime slutTid;
	private int deltagere;

	public Rundvisning(LocalDate dato, LocalTime startTid, LocalTime slutTid, int deltagere) {
		super("Rundvisning");
		this.dato = dato;
		this.startTid = startTid;
		this.slutTid = slutTid;
		this.deltagere = deltagere;
	}

	public LocalDate getDato() {
		return dato;
	}

	public LocalTime getStartTid() {
		return startTid;
	}

	public LocalTime getSlutTid() {
		return slutTid;
	}

	public int getDeltagere() {
		return deltagere;
	}

	@Override
	public String toString() {
		return "Rundvisning den. " + dato + " starter kl: " + startTid + " slutter kl: " + slutTid + " " + "deltagere: "
				+ deltagere;
	}

	@Override
	public int compareTo(Rundvisning o) {
		return dato.compareTo(o.dato);
	}
}
