package guifx;

import application.model.Pris;
import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Sampakning;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class SampakningController {
	private Service service = Service.getService();

	@FXML
	private ListView<Produktgruppe> produktgrupperlvw;

	@FXML
	private ListView<Produkt> produkterlvw, sampakningProdukterlvw;

	@FXML
	private ListView<Pris> sampakningerlvw;

	@FXML
	private Button opretSampakningbtn, addProduktbtn, afbrydbtn;

	@FXML
	private TextField antaltxf;

	@FXML
	private Label sampakningErrorlbl;

	@FXML
	public void initialize() {
		ChangeListener<Produktgruppe> pglistener = (ov, oldpg, newpg) -> valgtProduktgruppeÆndret();
		produktgrupperlvw.getSelectionModel().selectedItemProperty().addListener(pglistener);
		sampakningerlvw.getItems().setAll(service.getSampakningspriser());
		produktgrupperlvw.getItems().setAll(service.getProduktgrupper());
		sampakningErrorlbl.setTextFill(Color.RED);
		textFormatter();
	}

	@FXML
	void createSampakning(ActionEvent event) {
		if (sampakningerlvw.getSelectionModel().getSelectedItem() == null) {
			sampakningErrorlbl.setText("Du skal vælge en sampakning");
		} else if (sampakningProdukterlvw.getItems().isEmpty()) {
			sampakningErrorlbl.setText("Du kan ikke lave en tom sampakning");
		} else {
			Sampakning sampakning = (Sampakning) sampakningerlvw.getSelectionModel().getSelectedItem().getProdukt();
			Pris pris = new Pris(sampakningerlvw.getSelectionModel().getSelectedItem().getNormalPris(), 0, sampakning);
			for (Produkt p : sampakningProdukterlvw.getItems()) {
				sampakning.addProdukt(p);
			}
			GUIFXController.salg.createSalgslinje(1, pris);
			GUIFXController.stage.close();
		}
	}

	@FXML
	void addProduktToSampakning(ActionEvent event) {
		if (antaltxf.getText().length() <= 0) {
			sampakningErrorlbl.setText("Du skal vælge et ental");
		} else if (Integer.parseInt(antaltxf.getText()) <= 0) {
			sampakningErrorlbl.setText("Antal skal være over 0");
		} else {
			int i = 0;
			while (i < Integer.parseInt(antaltxf.getText())) {
				sampakningProdukterlvw.getItems().add(produkterlvw.getSelectionModel().getSelectedItem());
				i++;
			}
			sampakningErrorlbl.setText("");
		}
	}

	private void valgtProduktgruppeÆndret() {
		produkterlvw.getItems().setAll(produktgrupperlvw.getSelectionModel().getSelectedItem().getProdukter());
	}

	@FXML
	void afbryd(ActionEvent event) {
		GUIFXController.stage.close();
	}

	private void textFormatter() {
		antaltxf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					antaltxf.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}

}
