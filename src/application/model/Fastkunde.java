package application.model;

public class Fastkunde {
	private String navn;
	private double rabat;

	/**
	 * Initialiser fastkunde med navn og rabat. rabat > 0 & rabat <= 100
	 * 
	 * @param navn
	 * @param rabat
	 */
	public Fastkunde(String navn, double rabat) {
		this.navn = navn;
		setRabat(rabat);
	}

	public double getRabat() {
		return rabat;
	}

	public void setRabat(double rabat) {
		if (rabat > 0 && rabat <= 100) {
			this.rabat = rabat;
		}
	}

	@Override
	public String toString() {
		return navn + " " + rabat + " %";
	}

}
