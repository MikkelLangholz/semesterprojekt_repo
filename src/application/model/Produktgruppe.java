package application.model;

import java.util.ArrayList;
import java.util.List;

public class Produktgruppe {
	private String navn;
	private List<Produkt> produkter = new ArrayList<>();

	public Produktgruppe(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return navn;
	}

	public List<Produkt> getProdukter() {
		return new ArrayList<>(produkter);
	}

	public void addProdukt(Produkt produkt) {
		produkter.add(produkt);
	}

	public void removeProdukt(Produkt produkt) {
		produkter.remove(produkt);
	}

	@Override
	public String toString() {
		return navn;
	}
}
