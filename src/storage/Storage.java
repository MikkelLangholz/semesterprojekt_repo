package storage;

import java.util.ArrayList;
import java.util.List;

import application.model.Fastkunde;
import application.model.Pris;
import application.model.Prisliste;
import application.model.Produktgruppe;
import application.model.Salg;

public class Storage {

	private static Storage storage;
	private List<Salg> salgsliste = new ArrayList<>();
	private List<Salg> udlejninger = new ArrayList<>();
	private List<Produktgruppe> produktgrupper = new ArrayList<>();
	private List<Fastkunde> fastkunder = new ArrayList<>();
	private List<Prisliste> prislister = new ArrayList<>();
	private List<Pris> anlægpriser = new ArrayList<>();
	private List<Pris> rundvisningspriser = new ArrayList<>();
	private List<Pris> sampakningspriser = new ArrayList<>();

	private Storage() {
	}

	public static Storage getStorage() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}

	public List<Salg> getSalgsliste() {
		return new ArrayList<>(salgsliste);
	}

	public void addSalg(Salg salg) {
		salgsliste.add(salg);
	}

	public void removeSalg(Salg salg) {
		salgsliste.remove(salg);
	}

	public List<Salg> getUdlejninger() {
		return new ArrayList<>(udlejninger);
	}

	public void addUdlejning(Salg udlejning) {
		udlejninger.add(udlejning);
	}

	public void removeUdlejning(Salg udlejning) {
		udlejninger.remove(udlejning);
	}

	public List<Produktgruppe> getProduktgrupper() {
		return new ArrayList<>(produktgrupper);
	}

	public void addProduktgruppe(Produktgruppe produktgruppe) {
		produktgrupper.add(produktgruppe);
	}

	public void removeProduktgruppe(Produktgruppe produktgruppe) {
		produktgrupper.remove(produktgruppe);
	}

	public List<Fastkunde> getFastkunder() {
		return new ArrayList<>(fastkunder);
	}

	public void addFastkunde(Fastkunde fastkunde) {
		fastkunder.add(fastkunde);
	}

	public void removeFastkunde(Fastkunde fastkunde) {
		fastkunder.remove(fastkunde);
	}

	public List<Prisliste> getPrislister() {
		return new ArrayList<>(prislister);
	}

	public void addPrisliste(Prisliste prisliste) {
		prislister.add(prisliste);
	}

	public void removePrisliste(Prisliste prisliste) {
		prislister.remove(prisliste);
	}

	public List<Pris> getAnlægspriser() {
		return new ArrayList<>(anlægpriser);
	}

	public void addAnlægspris(Pris pris) {
		anlægpriser.add(pris);
	}

	public List<Pris> getRundvisningspriser() {
		return new ArrayList<>(rundvisningspriser);
	}

	public void addRundvisningspris(Pris pris) {
		rundvisningspriser.add(pris);
	}

	public List<Pris> getSampakningspriser() {
		return new ArrayList<>(sampakningspriser);
	}

	public void addSampakningspris(Pris pris) {
		sampakningspriser.add(pris);
	}
}