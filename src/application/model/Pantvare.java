package application.model;

public class Pantvare extends Produkt {
	private double pant;

	public Pantvare(String navn, double pant) {
		super(navn);
		this.pant = pant;
	}

	public double getPant() {
		return pant;
	}

	@Override
	public String toString() {
		return super.toString() + " + pant " + pant + " pris: ";
	}

}
