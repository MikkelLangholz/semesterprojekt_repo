package application.model;

public class Salgslinje {
	private int antal;
	private Pris pris;

	public Salgslinje(int antal, Pris pris) {
		this.antal = antal;
		this.pris = pris;
	}

	public int getAntal() {
		return antal;
	}

	/*
	 * reducere antal med 1
	 */
	public void antalMinusEt() {
		antal = antal - 1;
	}

	public Pris getPris() {
		return pris;
	}

	/**
	 * Beregner prisen på salgslinjen Medregner pant hvis produktet er en pantvare
	 * ganger med antal af produkter
	 * 
	 * @return pris
	 */
	public double calcPris() {
		double calcPris = pris.getNormalPris() * antal;
		if (pris.getProdukt() instanceof Pantvare) {
			Pantvare pv = (Pantvare) pris.getProdukt();
			calcPris += pv.getPant() * antal;
		}
		return calcPris;
	}

	@Override
	public String toString() {
		return pris + " " + antal + " stk";
	}

}
