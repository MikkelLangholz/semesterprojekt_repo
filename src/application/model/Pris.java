package application.model;

public class Pris {
	private double normalPris;
	private int klipPris;
	private Produkt produkt;

	public Pris(double normalPris, int klipPris, Produkt produkt) {
		this.normalPris = normalPris;
		this.klipPris = klipPris;
		this.produkt = produkt;
	}

	public double getNormalPris() {
		return normalPris;
	}

	public int getKlipPris() {
		return klipPris;
	}

	public Produkt getProdukt() {
		return produkt;
	}

	@Override
	public String toString() {
		return "" + produkt + " " + normalPris + " kr";
	}
}
