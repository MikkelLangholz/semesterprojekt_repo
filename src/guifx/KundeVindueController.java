package guifx;

import application.model.Fastkunde;
import application.service.Service;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;

public class KundeVindueController {
	private Service service = Service.getService();

	@FXML
	private Label fastkundeErrorlbl;

	@FXML
	private Button vælgbtn, afbrydbtn;

	@FXML
	private ListView<Fastkunde> fastkundelvw;

	@FXML
	public void initialize() {
		fastkundelvw.getItems().setAll(service.getFastekunder());
		fastkundeErrorlbl.setTextFill(Color.RED);
	}

	@FXML
	void vælgKunde(ActionEvent event) {
		if (fastkundelvw.getSelectionModel().getSelectedItem() == null) {
			fastkundeErrorlbl.setText("Du skal vælge en kunde");
		} else {
			GUIFXController.salg.setFastkunde(fastkundelvw.getSelectionModel().getSelectedItem());
			GUIFXController.stage.close();
		}
	}

	@FXML
	void afbryd(ActionEvent event) {
		GUIFXController.stage.close();
	}
}
