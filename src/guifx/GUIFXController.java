package guifx;

import java.io.IOException;
import java.time.LocalTime;
import java.util.Collections;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import application.model.Betaling;
import application.model.Betalingsmetode;
import application.model.Pantvare;
import application.model.Pris;
import application.model.Prisliste;
import application.model.Produkt;
import application.model.Produktgruppe;
import application.model.Rundvisning;
import application.model.Salg;
import application.model.Salgslinje;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class GUIFXController {
	private Service service = Service.getService();
	static Stage stage;
	static Salg salg;

	@FXML
	private Label salgErrorlbl, adminErrorlbl, udlejningErrorlbl;

	@FXML
	private TextField manglendepPristxf, antaltxf, produktgruppenavntxf, produktnavntxf, produktpristxf,
			prislisteNavntxf, betalingtxf, pristxf, betalingudlejningtxf, startTidtxf, slutTidtxf,
			rundvisningdeltagertxf, antalSolgttxf, adminKlipPristxf, klipPristxf, solgteKliptxf, brugteKliptxf, panttxf,
			fastkundenavn, rabattxf;

	@FXML
	private Button sletSalgslinjebtn, startUdlejningbtn, tilføjSalgslinjebtn, vælgKundebtn, klipBetaltbn,
			opretSampakingbtn;

	@FXML
	private ListView<Salg> udlejningerlvw, salglvw;

	@FXML
	private ListView<Salgslinje> salgslinjerlvw, salgslinjerudlejningerlvw, salgslinjerstatistiklvw;

	@FXML
	private ListView<Produktgruppe> salgProduktgrupperlvw, adminProduktgrupperlvw, statProduktgrupperlvw;

	@FXML
	private ListView<Produkt> adminProdukterlvw, statProdukerlvw;

	@FXML
	private ListView<Prisliste> prislisterlvw;

	@FXML
	private ListView<Pris> priserlvw;

	@FXML
	private ListView<Rundvisning> rundvisninglvw;

	@FXML
	private ListView<Betaling> betalingerlvw;

	@FXML
	private ChoiceBox<Betalingsmetode> salgBetalingsmetodechb, udlejningBetalingsmetodechb;

	@FXML
	private ChoiceBox<Prisliste> prislisterchb;

	@FXML
	private DatePicker rundvisningDate, startDato, slutDato, salgDato;

	@FXML
	public void initialize() {
		// Init salg
		salg = service.createSalg();
		salgBetalingsmetodechb.getItems().setAll(Betalingsmetode.values());
		salgBetalingsmetodechb.getItems().remove(Betalingsmetode.KLIPPEKORT);
		salgBetalingsmetodechb.getSelectionModel().selectFirst();
		prislisterchb.getItems().setAll(service.getPrislister());
		prislisterchb.getSelectionModel().selectFirst();
		valgtPrislisteÆndret();
		klipPristxf.setEditable(false);
		ChangeListener<Produktgruppe> pglistener = (ov, oldpg, newpg) -> valgtProduktgruppeÆndret();
		salgProduktgrupperlvw.getSelectionModel().selectedItemProperty().addListener(pglistener);
		manglendepPristxf.textProperty().addListener((ov, oldt, newt) -> betalprisÆndret());
		ChangeListener<Prisliste> pllistener = (ov, oldpl, newpl) -> valgtPrislisteÆndret();
		prislisterchb.getSelectionModel().selectedItemProperty().addListener(pllistener);
		ChangeListener<Salgslinje> sllistener = (ov, oldsl, newsl) -> valgtSalgslinjeÆndret();
		salgslinjerlvw.getSelectionModel().selectedItemProperty().addListener(sllistener);
		salgErrorlbl.setTextFill(Color.RED);
		textFormatters();
		// Init admin
		adminProduktgrupperlvw.getItems().setAll(service.getProduktgrupper());
		ChangeListener<Produktgruppe> adminPglistener = (ov, oldpg, newpg) -> valgtAdminProduktgruppeÆndret();
		adminProduktgrupperlvw.getSelectionModel().selectedItemProperty().addListener(adminPglistener);
		prislisterlvw.getItems().setAll(service.getPrislister());
		adminErrorlbl.setTextFill(Color.RED);
		// Init stat
		statProduktgrupperlvw.getItems().setAll(service.getProduktgrupper());
		ChangeListener<Produktgruppe> statPglistener = (ov, oldpg, newpg) -> valgtStatProduktgruppeÆndret();
		statProduktgrupperlvw.getSelectionModel().selectedItemProperty().addListener(statPglistener);
		ChangeListener<Produkt> plistener = (ov, oldp, newp) -> valgtProduktÆndret();
		statProdukerlvw.getSelectionModel().selectedItemProperty().addListener(plistener);
		antalSolgttxf.setEditable(false);
		solgteKliptxf.setEditable(false);
		brugteKliptxf.setEditable(false);
		// Init salg statitisk
		ChangeListener<Salg> salgStatistiklistener = (ov, oldsalg, newsalg) -> valgtSalgStatistiskÆndret();
		salglvw.getSelectionModel().selectedItemProperty().addListener(salgStatistiklistener);
		// Init udlejning
		udlejningBetalingsmetodechb.getItems().setAll(Betalingsmetode.values());
		udlejningBetalingsmetodechb.getItems().remove(Betalingsmetode.KLIPPEKORT);
		udlejningBetalingsmetodechb.getSelectionModel().selectFirst();
		udlejningerlvw.getItems().setAll(service.getUdlejninger());
		ChangeListener<Salg> salglistener = (ov, oldsalg, newsalg) -> valgtSalgÆndret();
		udlejningerlvw.getSelectionModel().selectedItemProperty().addListener(salglistener);
		udlejningErrorlbl.setTextFill(Color.RED);
		pristxf.setEditable(false);
	}

	// Salg vindue metoder
	// ---------------------------------------------------------------------------------------------------

	/*
	 * Opretter betaling. Giver fejlbesked hvis betalingtxf ikke indeholder tekst.
	 * Hvis salg.manglendePris bliver sat til under 0 afsluttes salg og gemmes på en
	 * ny liste. Disabler knapper under betaling for ikke at forvirre rabatudregning
	 * 
	 */
	@FXML
	void createBetaling(ActionEvent event) {
		if (betalingtxf.getText().length() <= 0) {
			salgErrorlbl.setText("Hvor meget vil du betale?");
		} else {
			salg.createBetaling(salgBetalingsmetodechb.getSelectionModel().getSelectedItem(),
					Double.parseDouble(betalingtxf.getText()));
			tilføjSalgslinjebtn.setDisable(true);
			sletSalgslinjebtn.setDisable(true);
			vælgKundebtn.setDisable(true);
			startUdlejningbtn.setDisable(true);
			klipBetaltbn.setDisable(true);
			opretSampakingbtn.setDisable(true);
			if (salg.getManglendePris() <= 0) {
				Salg gemtsalg = salg;
				service.finishSalg(gemtsalg);
				salg = service.createSalg();
				salgslinjerlvw.getItems().clear();
				manglendepPristxf.clear();
				tilføjSalgslinjebtn.setDisable(false);
				sletSalgslinjebtn.setDisable(false);
				vælgKundebtn.setDisable(false);
				startUdlejningbtn.setDisable(false);
				klipBetaltbn.setDisable(false);
				opretSampakingbtn.setDisable(false);
			}
			manglendepPristxf.setText(salg.calcManglendePris() + "");
			betalingtxf.setText("");
			salgErrorlbl.setText("");
		}
	}

	/*
	 * Opretter salgslinje Giver fejlsbesked hvis der ikke er valgt en pris eller
	 * ikke er sat et validt antal.
	 */
	@FXML
	void createSalgslinje(ActionEvent event) {
		if (priserlvw.getSelectionModel().getSelectedItem() == null) {
			salgErrorlbl.setText("Du skal vælge et produkt");
		} else if (antaltxf.getText().length() <= 0) {
			salgErrorlbl.setText("Du skal vælge et antal");
		} else if (Integer.parseInt(antaltxf.getText()) <= 0) {
			salgErrorlbl.setText("Antal skal være mere end 0");
		} else {
			salg.createSalgslinje(Integer.parseInt(antaltxf.getText()),
					priserlvw.getSelectionModel().getSelectedItem());
			updateSalgslinjer();
			salgErrorlbl.setText("");
		}
	}

	@FXML
	void removeSalgslinje(ActionEvent event) {
		salg.removeSalgslinje(salgslinjerlvw.getSelectionModel().getSelectedItem());
		updateSalgslinjer();
	}

	/*
	 * Opretter nyt salg ved brug af klippekort
	 */
	@FXML
	void betalKlip() {
		if (salgslinjerlvw.getSelectionModel().getSelectedItem() == null) {
			salgErrorlbl.setText("Du skal vælge en salgslinje");
		} else if (salgslinjerlvw.getSelectionModel().getSelectedItem().getPris().getKlipPris() == 0) {
			salgErrorlbl.setText("Det produkt har ikke nogen klippris");
		} else {
			if (salgslinjerlvw.getSelectionModel().getSelectedItem().getPris().getKlipPris() > 0) {
				Salg klipSalg = service.createSalg();
				Pris pris = new Pris(0, salgslinjerlvw.getSelectionModel().getSelectedItem().getPris().getKlipPris(),
						salgslinjerlvw.getSelectionModel().getSelectedItem().getPris().getProdukt());
				klipSalg.createSalgslinje(salgslinjerlvw.getSelectionModel().getSelectedItem().getAntal(), pris);
				klipSalg.createBetaling(Betalingsmetode.KLIPPEKORT, 0);
				salg.removeSalgslinje(salgslinjerlvw.getSelectionModel().getSelectedItem());
				service.finishSalg(klipSalg);
				salgslinjerlvw.getItems().remove(salgslinjerlvw.getSelectionModel().getSelectedItem());
				manglendepPristxf.setText(salg.calcManglendePris() + "");
				salgErrorlbl.setText("");
			}
		}
	}

	/*
	 * Åbner nyt vindue sætter. Opdatere samletPris
	 */
	@FXML
	void vælgKundevindue(ActionEvent event) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Kundevindue.fxml"));
		Parent root = fxmlLoader.load();
		stage = new Stage();
		stage.setOpacity(1);
		stage.setTitle("Faste kunder");
		stage.setScene(new Scene(root, 250, 450));
		stage.showAndWait();
		updateSamletPris();
	}

	/*
	 * starter udlejningsvindue
	 */
	@FXML
	void startUdlejning(ActionEvent event) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Udlejningsvindue.fxml"));
		Parent root = fxmlLoader.load();
		stage = new Stage();
		stage.setOpacity(1);
		stage.setTitle("Udlejning");
		stage.setScene(new Scene(root, 580, 400));
		stage.showAndWait();
		udlejningerlvw.getItems().setAll(service.getUdlejninger());
		if (UdlejningsVindueController.isCompleted == true) {
			manglendepPristxf.clear();
			salgslinjerlvw.getItems().clear();
			UdlejningsVindueController.isCompleted = false;
		}
	}

	/*
	 * åbner vindue til oprettelse af sampakning
	 */
	@FXML
	void createSampakningVindue(ActionEvent Event) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SampakningVindue.fxml"));
		Parent root = fxmlLoader.load();
		stage = new Stage();
		stage.setOpacity(1);
		stage.setTitle("Sampakninger");
		stage.setScene(new Scene(root, 650, 450));
		stage.showAndWait();
		salgslinjerlvw.getItems().setAll(salg.getSalgslinjer());
		manglendepPristxf.setText(salg.calcSamletPris() + "");
	}

	private void updateSalgslinjer() {
		salgslinjerlvw.getItems().setAll(salg.getSalgslinjer());
		updateSamletPris();
	}

	private void updateSamletPris() {
		manglendepPristxf.setText("" + salg.calcSamletPris());
	}

	private void valgtPrislisteÆndret() {
		if (prislisterchb.getSelectionModel().getSelectedItem() != null) {
			updateProduktgrupper();
		}
	}

	private void updateProduktgrupper() {
		salgProduktgrupperlvw.getItems().clear();
		priserlvw.getItems().clear();
		for (Pris p : prislisterchb.getSelectionModel().getSelectedItem().getPriser()) {
			if (!salgProduktgrupperlvw.getItems().contains(p.getProdukt().getProduktgruppe())) {
				salgProduktgrupperlvw.getItems().add(p.getProdukt().getProduktgruppe());
			}
		}
	}

	private void valgtProduktgruppeÆndret() {
		priserlvw.getItems().clear();

		priserlvw.getItems().setAll(service.getPriser(prislisterchb.getSelectionModel().getSelectedItem(),
				salgProduktgrupperlvw.getSelectionModel().getSelectedItem()));
	}

	/*
	 * Ændre den manglendepris på et salg hvis brugeren har brug for manuelt at
	 * ændre prisen på et salg
	 */
	private void betalprisÆndret() {
		if (manglendepPristxf.getText().length() > 0) {
			salg.setManglendePris(Double.parseDouble(manglendepPristxf.getText()));
		} else {
			salg.setManglendePris(0);
		}
	}

	private void valgtSalgslinjeÆndret() {
		klipPristxf.clear();
		if (salgslinjerlvw.getSelectionModel().getSelectedItem() != null
				&& salgslinjerlvw.getSelectionModel().getSelectedItem().getPris().getKlipPris() > 0) {
			klipPristxf.setText(salgslinjerlvw.getSelectionModel().getSelectedItem().getPris().getKlipPris()
					* salgslinjerlvw.getSelectionModel().getSelectedItem().getAntal() + "");

		}
	}

	/*
	 * kodet taget fra kilder:
	 * https://stackoverflow.com/questions/7555564/what-is-the-recommended-way-to-
	 * make-a-numeric-textfield-in-javafx?utm_medium=organic&utm_source=
	 * google_rich_qa&utm_campaign=google_rich_qa
	 * 
	 * https://stackoverflow.com/questions/43307872/javafx-textfield-limit-character
	 * -input-to-double-no-multiple-commas?utm_medium=organic&utm_source=
	 * google_rich_qa&utm_campaign=google_rich_qa
	 * 
	 */
	private void textFormatters() {
		antaltxf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					antaltxf.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
		rundvisningdeltagertxf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					antaltxf.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});

		rabattxf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					antaltxf.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});

		adminKlipPristxf.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					antaltxf.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});

		Pattern pattern = Pattern.compile("\\d*|\\d+\\.\\d*");
		TextFormatter<?> formatter1 = new TextFormatter<Object>((UnaryOperator<TextFormatter.Change>) change -> {
			return pattern.matcher(change.getControlNewText()).matches() ? change : null;
		});
		TextFormatter<?> formatter2 = new TextFormatter<Object>((UnaryOperator<TextFormatter.Change>) change -> {
			return pattern.matcher(change.getControlNewText()).matches() ? change : null;
		});
		TextFormatter<?> formatter3 = new TextFormatter<Object>((UnaryOperator<TextFormatter.Change>) change -> {
			return pattern.matcher(change.getControlNewText()).matches() ? change : null;
		});
		TextFormatter<?> formatter4 = new TextFormatter<Object>((UnaryOperator<TextFormatter.Change>) change -> {
			return pattern.matcher(change.getControlNewText()).matches() ? change : null;
		});
		TextFormatter<?> formatter5 = new TextFormatter<Object>((UnaryOperator<TextFormatter.Change>) change -> {
			return pattern.matcher(change.getControlNewText()).matches() ? change : null;
		});
		manglendepPristxf.setTextFormatter(formatter1);
		betalingtxf.setTextFormatter(formatter2);
		produktpristxf.setTextFormatter(formatter3);
		betalingudlejningtxf.setTextFormatter(formatter4);
		panttxf.setTextFormatter(formatter5);
	}

	// Admin metoder
	// ---------------------------------------------------------------------------------------------------
	@FXML
	void createKategori(ActionEvent event) {
		if (produktgruppenavntxf.getText().length() <= 0) {
			adminErrorlbl.setText("Kategorien skal have et navn");
		} else {
			service.createProduktgruppe(produktgruppenavntxf.getText());
			adminProduktgrupperlvw.getItems().setAll(service.getProduktgrupper());
			statProduktgrupperlvw.getItems().setAll(service.getProduktgrupper());
			produktgruppenavntxf.clear();
			adminErrorlbl.setText("");
		}
	}

	/*
	 * Opretter et produkt. Hvis pant feltet er udfyldt oprettes en pantvare i
	 * stedet
	 */
	@FXML
	void createProdukt(ActionEvent event) {
		if (produktnavntxf.getText().length() <= 0) {
			adminErrorlbl.setText("Produktet skal have et navn");
		} else if (adminProduktgrupperlvw.getSelectionModel().getSelectedItem() == null) {
			adminErrorlbl.setText("Du skal vælge en produktgruppe");
		} else if (panttxf.getText().length() <= 0) {
			Produkt p = service.createProdukt(produktnavntxf.getText(),
					adminProduktgrupperlvw.getSelectionModel().getSelectedItem());
			adminProdukterlvw.getItems().add(p);
			produktnavntxf.clear();
			adminErrorlbl.setText("");
		} else {
			Pantvare pv = service.createPantvare(produktnavntxf.getText(), Double.parseDouble(panttxf.getText()),
					adminProduktgrupperlvw.getSelectionModel().getSelectedItem());
			adminProdukterlvw.getItems().add(pv);
			produktnavntxf.clear();
			panttxf.clear();
			adminErrorlbl.setText("");
		}
	}

	/*
	 * Opretter en pris. Sætter klippris til 0 (ingenting) hvis feltet ikke er
	 * udfyldt
	 */
	@FXML
	void createPris(ActionEvent event) {
		if (produktpristxf.getText().length() <= 0) {
			adminErrorlbl.setText("Produktet skal have en pris");
		} else if (Double.parseDouble(produktpristxf.getText()) <= 0) {
			adminErrorlbl.setText("Prisen skal være over 0");
		} else if (prislisterlvw.getSelectionModel().getSelectedItem() == null) {
			adminErrorlbl.setText("Vælg en prisliste");
		} else if (adminProdukterlvw.getSelectionModel().getSelectedItem() == null) {
			adminErrorlbl.setText("Du skal vælge et produkt");
		} else {

			if (adminKlipPristxf.getText().length() > 0) {
				prislisterlvw.getSelectionModel().getSelectedItem().createPris(
						Double.parseDouble(produktpristxf.getText()), Integer.parseInt(adminKlipPristxf.getText()),
						adminProdukterlvw.getSelectionModel().getSelectedItem());
				adminErrorlbl.setText("");
			} else {
				prislisterlvw.getSelectionModel().getSelectedItem().createPris(
						Double.parseDouble(produktpristxf.getText()), 0,
						adminProdukterlvw.getSelectionModel().getSelectedItem());
				adminErrorlbl.setText("");
			}
			valgtPrislisteÆndret();
			produktpristxf.clear();
		}
	}

	@FXML
	void createPrisliste(ActionEvent event) {
		if (prislisteNavntxf.getText().length() <= 0) {
			adminErrorlbl.setText("Prislisten skal have et navn");
		} else {
			service.createPrisliste(prislisteNavntxf.getText());
			prislisterlvw.getItems().setAll(service.getPrislister());
			prislisteNavntxf.clear();
			prislisterchb.getItems().setAll(service.getPrislister());
			prislisterchb.getSelectionModel().selectFirst();
			adminErrorlbl.setText("");
		}
	}

	@FXML
	void createFastkunde(ActionEvent event) {
		if (fastkundenavn.getText().length() <= 0) {
			adminErrorlbl.setText("En fastkunde skal have et navn");
		} else if (rabattxf.getText().length() <= 0) {
			adminErrorlbl.setText("En fastkunde skal have en rabat");
		} else {
			service.createFastkunde(fastkundenavn.getText(), Integer.parseInt(rabattxf.getText()));
			fastkundenavn.clear();
			rabattxf.clear();
			adminErrorlbl.setText("");
		}
	}

	/*
	 * Opretter rundvisning med pris. Prisen sættes til antal deltagere * 100
	 */
	@FXML
	void createRundvisning(ActionEvent event) {
		if (rundvisningDate.getValue() == null) {
			adminErrorlbl.setText("Vælg en dato");
		} else if (rundvisningdeltagertxf.getText().length() <= 0) {
			adminErrorlbl.setText("Vælg et antal deltagere");
		} else if (startTidtxf.getText().length() <= 0) {
			adminErrorlbl.setText("Vælg en start tid");
		} else if (slutTidtxf.getText().length() <= 0) {
			adminErrorlbl.setText("Vælg en slut tid");
		} else {
			Rundvisning rundvisning = service.createRundvisning(rundvisningDate.getValue(),
					LocalTime.parse(startTidtxf.getText()), LocalTime.parse(slutTidtxf.getText()),
					Integer.parseInt(rundvisningdeltagertxf.getText()));
			Pris pris = new Pris(Integer.parseInt(rundvisningdeltagertxf.getText()) * 100, 0, rundvisning);
			salg.createSalgslinje(1, pris);
			updateSalgslinjer();
			rundvisningDate.getEditor().clear();
			rundvisningDate.setValue(null);
			startTidtxf.clear();
			slutTidtxf.clear();
			rundvisningdeltagertxf.clear();
			adminErrorlbl.setText("");
		}
		Collections.sort(rundvisninglvw.getItems());
	}

	private void valgtAdminProduktgruppeÆndret() {
		if (adminProduktgrupperlvw.getSelectionModel().getSelectedItem() != null) {
			adminProdukterlvw.getItems()
					.setAll(adminProduktgrupperlvw.getSelectionModel().getSelectedItem().getProdukter());
		}

	}

	// Statitisk metoder
	// ---------------------------------------------------------------------------------------------------
	@FXML
	void statistik(ActionEvent event) {
		valgtProduktÆndret();
		if (startDato.getValue() != null & slutDato.getValue() != null) {
			solgteKliptxf.setText(service.solgteKlip(startDato.getValue(), slutDato.getValue()) + "");
			brugteKliptxf.setText(service.brugteKlip(startDato.getValue(), slutDato.getValue()) + "");
			rundvisninglvw.getItems().setAll(service.getRundvisninger(startDato.getValue(), slutDato.getValue()));
		}
	}

	private void valgtStatProduktgruppeÆndret() {
		statProdukerlvw.getItems().setAll(statProduktgrupperlvw.getSelectionModel().getSelectedItem().getProdukter());
	}

	private void valgtProduktÆndret() {
		if (startDato.getValue() != null && slutDato.getValue() != null) {
			antalSolgttxf.setText(service.solgtProdukt(startDato.getValue(), slutDato.getValue(),
					statProdukerlvw.getSelectionModel().getSelectedItem()) + "");
		}
	}

	// salg statitisk metoder
	// ---------------------------------------------------------------------------------------------------

	@FXML
	void salgStatistisk() {
		salglvw.getItems().setAll(service.salgStatistik(salgDato.getValue()));
	}

	private void valgtSalgStatistiskÆndret() {
		if (salglvw.getSelectionModel().getSelectedItem() != null) {
			salgslinjerstatistiklvw.getItems().setAll(salglvw.getSelectionModel().getSelectedItem().getSalgslinjer());
			betalingerlvw.getItems().setAll(salglvw.getSelectionModel().getSelectedItem().getBetalinger());
		}
	}

	// Udlejninger metoder
	// ---------------------------------------------------------------------------------------------------
	@FXML
	void betalUdlejning() {
		if (betalingudlejningtxf.getText().length() <= 0) {
			udlejningErrorlbl.setText("Hvor meget vil du betale?");
		} else {
			udlejningerlvw.getSelectionModel().getSelectedItem().createBetaling(
					udlejningBetalingsmetodechb.getSelectionModel().getSelectedItem(),
					Double.parseDouble(betalingudlejningtxf.getText()));
			pristxf.setText(udlejningerlvw.getSelectionModel().getSelectedItem().calcManglendePris() + "");
			if (udlejningerlvw.getSelectionModel().getSelectedItem().getManglendePris() <= 0) {
				betalingudlejningtxf.setText("");
				salgslinjerudlejningerlvw.getItems().clear();
				service.finishSalg(udlejningerlvw.getSelectionModel().getSelectedItem());
				service.removeUdlejning(udlejningerlvw.getSelectionModel().getSelectedItem());
				udlejningerlvw.getItems().remove(udlejningerlvw.getSelectionModel().getSelectedItem());
				udlejningErrorlbl.setText("");
			}
		}
	}

	/*
	 * Reducere antal på en salgslinje med 1. Fjerner salgslinjen hvis antal er 0
	 */
	@FXML
	void fjernReturneredeVare() {
		if (salgslinjerudlejningerlvw.getSelectionModel().getSelectedItem() == null) {
			udlejningErrorlbl.setText("Du skal vælge en salgslinje");
		} else {
			if (salgslinjerudlejningerlvw.getSelectionModel().getSelectedItem().getAntal() > 1) {
				salgslinjerudlejningerlvw.getSelectionModel().getSelectedItem().antalMinusEt();
			} else if (salgslinjerudlejningerlvw.getSelectionModel().getSelectedItem().getAntal() == 1) {
				udlejningerlvw.getSelectionModel().getSelectedItem()
						.removeSalgslinje(salgslinjerudlejningerlvw.getSelectionModel().getSelectedItem());
			}
			pristxf.setText(udlejningerlvw.getSelectionModel().getSelectedItem().calcManglendePris() + "");
			int i = udlejningerlvw.getSelectionModel().getSelectedIndex();
			udlejningerlvw.getItems().setAll(service.getUdlejninger());
			udlejningerlvw.getSelectionModel().select(i);
			valgtSalgÆndret();
			udlejningErrorlbl.setText("");
		}
	}

	private void valgtSalgÆndret() {
		if (udlejningerlvw.getSelectionModel().getSelectedItem() != null) {
			pristxf.setText(udlejningerlvw.getSelectionModel().getSelectedItem().getManglendePris() + "");
			salgslinjerudlejningerlvw.getItems()
					.setAll(udlejningerlvw.getSelectionModel().getSelectedItem().getSalgslinjer());
		}
	}
}