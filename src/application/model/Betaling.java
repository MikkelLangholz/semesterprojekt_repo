package application.model;

public class Betaling {
	private Betalingsmetode betalingsmetode;
	private double beloeb;

	public Betaling(Betalingsmetode betalingsmetode, double beloeb) {
		this.betalingsmetode = betalingsmetode;
		this.beloeb = beloeb;
	}

	public Betalingsmetode getBetalingsmetode() {
		return betalingsmetode;
	}

	public double getBeloeb() {
		return beloeb;
	}

	@Override
	public String toString() {
		return betalingsmetode + " " + beloeb + " kr";
	}
}
