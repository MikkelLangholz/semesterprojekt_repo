package application.model;

public class Produkt {
	private static int nextID = 0;
	private int ID;
	private String navn;
	private Produktgruppe produktgruppe;

	public Produkt(String navn) {
		ID = nextID++;
		this.navn = navn;
	}

	public int getID() {
		return ID;
	}

	public Produktgruppe getProduktgruppe() {
		return produktgruppe;
	}

	public void setProduktgruppe(Produktgruppe produktgruppe) {
		this.produktgruppe = produktgruppe;
	}

	@Override
	public String toString() {
		return navn;
	}
}
