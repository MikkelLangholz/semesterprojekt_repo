package guifx;

import application.model.Betalingsmetode;
import application.model.Pantvare;
import application.model.Pris;
import application.model.Salgslinje;
import application.service.Service;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class UdlejningsVindueController {
	private Service service = Service.getService();
	static boolean isCompleted;

	@FXML
	private Label IDlbl, udlejningsErrorlbl;

	@FXML
	private TextField samletPristxf, panttxf, antaltxf;

	@FXML
	private Button betalPantbtn, tilføjbtn;

	@FXML
	private ListView<Salgslinje> salgslinjerlvw;

	@FXML
	private ListView<Pris> anlæglvw;

	@FXML
	private ChoiceBox<Betalingsmetode> betalingsmetodechb;

	@FXML
	public void initialize() {
		samletPristxf.setEditable(false);
		panttxf.setEditable(false);
		salgslinjerlvw.getItems().setAll(GUIFXController.salg.getSalgslinjer());
		anlæglvw.getItems().setAll(service.getAnlægspriser());
		samletPristxf.setText(GUIFXController.salg.getSamletPris() + "");
		IDlbl.setText(GUIFXController.salg.getID() + "");
		betalingsmetodechb.getItems().setAll(Betalingsmetode.values());
		betalingsmetodechb.getItems().remove(Betalingsmetode.KLIPPEKORT);
		betalingsmetodechb.getSelectionModel().selectFirst();
		panttxf.setText(getPant() + "");
		udlejningsErrorlbl.setTextFill(Color.RED);
	}

	@FXML
	void betalPant(ActionEvent event) {
		GUIFXController.salg.createBetaling(betalingsmetodechb.getSelectionModel().getSelectedItem(),
				Double.parseDouble(panttxf.getText()));
		GUIFXController.salg.calcManglendePris();
		service.addUdlejning(GUIFXController.salg);
		GUIFXController.salg = service.createSalg();
		isCompleted = true;
		GUIFXController.stage.close();
	}

	@FXML
	void createSalgslinje(ActionEvent event) {
		if (anlæglvw.getSelectionModel().getSelectedItem() == null) {
			udlejningsErrorlbl.setText("Du skal vælge et anlæg");
		} else if (antaltxf.getText().length() <= 0) {
			udlejningsErrorlbl.setText("Du skal vælge et antal");
		} else if (Integer.parseInt(antaltxf.getText()) <= 0) {
			udlejningsErrorlbl.setText("Antal skal være mere end 0");
		} else {
			Salgslinje salgslinje = GUIFXController.salg.createSalgslinje(Integer.parseInt(antaltxf.getText()),
					anlæglvw.getSelectionModel().getSelectedItem());
			salgslinjerlvw.getItems().add(salgslinje);
			samletPristxf.setText(GUIFXController.salg.calcSamletPris() + "");
			panttxf.setText(getPant() + "");
			udlejningsErrorlbl.setText("");
		}
	}

	private double getPant() {
		double pant = 0;
		for (Salgslinje sl : salgslinjerlvw.getItems()) {
			if (sl.getPris().getProdukt() instanceof Pantvare) {
				Pantvare pv = (Pantvare) sl.getPris().getProdukt();
				pant += pv.getPant() * sl.getAntal();
			}
		}
		return pant;
	}
}