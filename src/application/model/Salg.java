package application.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Salg {
	private static int nextID = 0;
	private int ID;
	private LocalDate dato;
	private double samletPris;
	private double manglendePris;
	private Fastkunde fastkunde;
	private List<Salgslinje> salgslinjer = new ArrayList<>();
	private List<Betaling> betalinger = new ArrayList<>();

	/**
	 * Initialiser salg. ID sættes automatisk til en unique værdi. Dato sættes til
	 * dagen salget blev oprettet
	 */
	public Salg() {
		ID = nextID++;
		this.dato = LocalDate.now();
	}

	public int getID() {
		return ID;
	}

	public void setFastkunde(Fastkunde fastkunde) {
		this.fastkunde = fastkunde;
	}

	public LocalDate getDato() {
		return dato;
	}

	public Fastkunde getFastkunde() {
		return fastkunde;
	}

	public double getSamletPris() {
		return samletPris;
	}

	public double getManglendePris() {
		return manglendePris;
	}

	public void setManglendePris(double manglendePris) {
		this.manglendePris = manglendePris;
	}

	public List<Salgslinje> getSalgslinjer() {
		return new ArrayList<>(salgslinjer);
	}

	/**
	 * Komposition. Opretter salgslinje på salg
	 * 
	 * @param antal
	 * @param pris
	 * @return salgslinje
	 */
	public Salgslinje createSalgslinje(int antal, Pris pris) {
		Salgslinje salgslinje = new Salgslinje(antal, pris);
		salgslinjer.add(salgslinje);
		return salgslinje;
	}

	public void removeSalgslinje(Salgslinje salgslinje) {
		salgslinjer.remove(salgslinje);
	}

	/**
	 * Komposition. Opretter betaling på salg
	 * 
	 * @param betalingsmetode
	 * @param beloeb
	 * @return betaling
	 */
	public Betaling createBetaling(Betalingsmetode betalingsmetode, double beloeb) {
		Betaling betaling = new Betaling(betalingsmetode, beloeb);
		manglendePris -= beloeb;
		betalinger.add(betaling);
		return betaling;
	}

	public List<Betaling> getBetalinger() {
		return new ArrayList<>(betalinger);
	}

	/**
	 * Beregner den samlede pris over alle salgslinjer Trækker rabat fra hvis
	 * fastkunde ikke er null
	 * 
	 * @return samletPris
	 */
	public double calcSamletPris() {
		samletPris = 0;
		for (Salgslinje sl : salgslinjer) {
			samletPris += sl.calcPris();
		}
		if (fastkunde != null) {
			double rabat = samletPris / 100 * fastkunde.getRabat();
			samletPris -= rabat;
		}
		return samletPris;
	}

	/**
	 * Beregner den manglendePris Sætter først manglendePris til samletPris og
	 * trækker derefter beloebet på alle betalinger fra manglendePris
	 * 
	 * @return manglendePris
	 */
	public double calcManglendePris() {
		manglendePris = calcSamletPris();
		for (Betaling b : betalinger) {
			manglendePris -= b.getBeloeb();
		}
		return manglendePris;
	}

	@Override
	public String toString() {
		return "ID: " + ID + " Pris: " + manglendePris;
	}
}
